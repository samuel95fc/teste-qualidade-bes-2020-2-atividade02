package atividade02;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;

@RunWith(Parameterized.class)
public class VerificarPrimoTest {

	private Questao01 questao01;
	private int input;
	private boolean expected;

	@Before
	public void initialize() {
		questao01 = new Questao01();
	}

	public VerificarPrimoTest(Integer input, Boolean expected) {
		this.input = input;
		this.expected = expected;
	}

	@Parameterized.Parameters
	public static Collection VerificarPrimo() {
		return Arrays.asList(new Object[][] {
			{ 2, true },
			{ 6, false },
			{ 7, true },
			{ 19, true },
			{ 22, false },
			{ 23, true },
			{ 56, false },
			{ 35, false },
			{ 11, true },
			{ 1000, false }
		});
	}

	@Test
	private void TestObterNumeroFaixa() {
		ByteArrayInputStream in = new ByteArrayInputStream("10\n".getBytes());

		System.setIn(in);

		Assert.assertEquals(Integer.valueOf(10), questao01.obterNumeroFaixa());
	}

	@Test
	private void TestVerificarPrimo() {
		Assert.assertTrue(questao01.verificarPrimo(input));
	}

	@Test
	private void TestExibirPrimoNaoPrimo() {
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		System.setOut(new PrintStream(out));

		questao01.exibirPrimoNaoPrimo(input, expected);

		Assert.assertEquals("O resultado da opera��o foi: ", out.toString());
	}
}
